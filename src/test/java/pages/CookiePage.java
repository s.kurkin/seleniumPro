package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class CookiePage extends BasePage {

    private final String USER_NAME = "Sergei Kurkin";
    private final String USER_EMAIL = "kurkin.serg@gmail.com";
    private final String USER_PASSWORD = "qaz123456789";

    private RemoteWebDriver driver;


    /*@FindBy(xpath = "//ul[@class='nav navbar-nav navbar-right']/li[5]")
    private WebElement logInButton;*/

    @FindBy(xpath = "//a[@class='navbar-link fedora-navbar-link']")
    private WebElement logInMainButton;

    @FindBy(xpath = "//input[@class='btn btn-primary btn-md login-button']")
    private WebElement logInButton;

    @FindBy(id = "user_name")
    private WebElement userName;

    @FindBy(id = "user_email")
    private WebElement userEmail;

    @FindBy(id = "user_password")
    private WebElement userPassword;

    @FindBy(id = "user_password_confirmation")
    private WebElement confirmPassword;

    @FindBy(id = "user_agreed_to_terms")
    private WebElement userAgreed;

    @FindBy(xpath = "//input[@class='btn btn-primary btn-md signup-button']")
    private WebElement signUpButton;

    @FindBy(xpath = "//a[@class='fedora-navbar-link navbar-link dropdown-toggle open-my-profile-dropdown']")
    private WebElement userSettings;

    @FindBy(xpath = "//li[@class='user-signout']/a")
    private WebElement logOutButton;

    public CookiePage(RemoteWebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(this.driver, this);
    }

    public void logInMainClick() {
        clickElement(logInMainButton);
    }

    public void logInClick() {
        clickElement(logInButton);
    }

    public void enterUserName() {
        sendKeysToElement(userName, USER_NAME);
    }

    public void enterUserEmail() {
        sendKeysToElement(userEmail, USER_EMAIL);
    }

    public void enterPassword() {
        sendKeysToElement(userPassword, USER_PASSWORD);
    }

    public void confirmPassword() {
        sendKeysToElement(confirmPassword, USER_PASSWORD);
    }

    public void agreePrivacyClick() {
        clickElement(userAgreed);
    }

    public void signUpClick() {
        clickElement(signUpButton);
    }

    public void userSettingsClick() {
        clickElement(userSettings);
    }

    public void logOutClick() {
        clickElement(logOutButton);
    }
}
