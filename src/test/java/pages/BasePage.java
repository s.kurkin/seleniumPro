package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public abstract class BasePage {
    private RemoteWebDriver driver;
    private WebDriverWait wait;

    public BasePage() {
        this.driver = driver;
        this.wait = wait;
    }

    public void clickElement(WebElement element) {
        wait.until(ExpectedConditions.elementToBeClickable(element)).click();
    }

    public void sendKeysToElement(WebElement element, String keys) {
        wait.until(ExpectedConditions.elementToBeClickable(element)).sendKeys(keys);
    }
}
