package helpers;

import org.apache.commons.lang3.SystemUtils;
import org.springframework.core.io.ClassPathResource;

import java.io.IOException;
import java.util.Properties;

public class DataHelper {

    public void setDriverProperty() throws IOException {
        Properties config = new Properties();
        config.load(new ClassPathResource("config.properties").getInputStream());

        if (SystemUtils.IS_OS_WINDOWS) {
            System.setProperty("webdriver.chrome.driver", config.getProperty("win.chromedriver.path"));
        } else if (SystemUtils.IS_OS_LINUX) {
            System.setProperty("webdriver.chrome.driver", config.getProperty("linux.chromedriver.path"));
        } else if (SystemUtils.IS_OS_MAC) {
            System.setProperty("webdriver.chrome.driver", config.getProperty("mac.chromedriver.path"));
        } else {
            throw new RuntimeException("Your OS " + SystemUtils.OS_NAME + " doesn't supported");
        }
    }
}
