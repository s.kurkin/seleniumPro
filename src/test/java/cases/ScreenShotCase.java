package cases;

import io.github.bonigarcia.wdm.ChromeDriverManager;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.List;
import java.util.concurrent.TimeUnit;

public class ScreenShotCase {

    /**
     * Задание2:
     * 1) В проекте с Allure добавить поддержку скриншотов, делать скриншот при падении теста
     * 2) Сделать падающий тест, чтобы получить отчёт со скриншотом
     */

    private static RemoteWebDriver driver;
    private static WebDriverWait wait;

    private final String YANDEX200_PAGE = "https://music.yandex.ru/album/4712278";
    private final String SONGS_LIST_XPATH = "//span[substring-before(text(),':')*60 + substring-after(text(),':')>200]/parent::div/parent::div";

    @BeforeClass
    public static void setUp() throws Exception {
        /*DataHelper dataHelper = new DataHelper();
        dataHelper.setDriverProperty();*/
        ChromeDriverManager.getInstance().version("2.32").setup();
        driver = new ChromeDriver();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        wait = new WebDriverWait(driver, 10);
        driver.manage().window().maximize();
    }

    @AfterClass
    public static void tearDown() throws Exception {
        driver.close();
        driver.quit();
    }

    @Test
    public void testMakeScreenShot() {
        driver.get(YANDEX200_PAGE);
        List<WebElement> songsList = driver.findElements(By.xpath(SONGS_LIST_XPATH));
        /*JUnitCore core = new JUnitCore();
        core.addListener(new CustomTestListener());
        core.run(CustomTestListener.class);*/
        Assert.assertEquals("Количество песен не 1000", 1000, songsList.size());
    }

    @Test
    public void testDriver() {
        driver.get(YANDEX200_PAGE);
        List<WebElement> songsList = driver.findElements(By.xpath(SONGS_LIST_XPATH));
        Assert.assertEquals(14, songsList.size());
    }
}
