package cases;

import io.github.bonigarcia.wdm.ChromeDriverManager;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.WebDriverWait;
import pages.CookiePage;

import java.util.concurrent.TimeUnit;

public class CookieCase {

    private final String COOKIE_PAGE = "http://courses.way2automation.com/";
    /**
     * Задание 3:
     * 1) Написать функцию записи куков в файл и считывания куков из него.
     * 2) Написать тест, авторизующийся на сайте http://courses.way2automation.com/ с помощью формы авторизации при первом запуске
     * и использующий куки при втором
     */

    private RemoteWebDriver driver;
    private WebDriverWait wait;
    private CookiePage cookiePage = new CookiePage(driver);

    @BeforeClass
    public void setUp() throws Exception {
        //config = config.load(new ClassPathResource("config.properties").getInputStream());
        ChromeDriverManager.getInstance().version("2.32").setup();
        driver = new ChromeDriver();
        wait = new WebDriverWait(driver, 10);
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        driver.manage().window().maximize();
    }

    @Test
    public void testCookie() {
        driver.get(COOKIE_PAGE);
        LogInOnMainPage(driver);
        LogOut(driver);
        SignUpWithCookie(driver);
    }

    private void SignUpWithCookie(RemoteWebDriver driver) {
        //CookiePage cookiePage = new CookiePage(driver, wait);

    }

    private void LogOut(RemoteWebDriver driver) {
        cookiePage.userSettingsClick();
        cookiePage.logOutClick();
    }

    private void SignUpOnMainPage(RemoteWebDriver driver) {
        cookiePage.logInMainClick();
        cookiePage.enterUserName();
        cookiePage.enterUserEmail();
        cookiePage.enterPassword();
        cookiePage.confirmPassword();
        cookiePage.agreePrivacyClick();
        cookiePage.signUpClick();
    }

    private void LogInOnMainPage(RemoteWebDriver driver) {
        cookiePage.logInMainClick();
        cookiePage.enterUserEmail();
        cookiePage.enterPassword();
        cookiePage.logInClick();

    }

    @AfterClass
    public void tearDown() throws Exception {
        driver.close();
        driver.quit();
    }
}
