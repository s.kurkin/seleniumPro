import cases.CookieCase;
import cases.ScreenShotCase;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;

@RunWith(Suite.class)
@Suite.SuiteClasses({
        ScreenShotCase.class,
        CookieCase.class
})
public class RunningAllSuiteTest {

}
